# to-be-continuous synchronization

This project allows to recursively copy/synchronize the [to be continuous](https://gitlab.com/to-be-continuous) project from gitlab.com to your self-managed GitLab instance.

It can be run manually (manual pipeline) and also as scheduled CI/CD pipeline to regularly retrieve upstream changes.

Under the hood, it uses the [GitLab Copy CLI](https://gitlab.com/to-be-continuous/tools/gitlab-cp) tool as a Docker image, [directly pulled from gitlab.com](https://gitlab.com/to-be-continuous/tools/gitlab-cp/container_registry/6261230).

## Usage: CLI (first-time copy)

The GitLab Copy CLI tool can be used directly from your computer (this is the recommended way to clone _to be continuous_ for the first time).

More info is available in [_to be continuous_ documentation](https://to-be-continuous.gitlab.io/doc/self-managed/basic/#copy-tbc-to-your-gitlab).

## Usage: CI/CD

Once copied _to be continuous_ to your GitLab server, you shall then schedule a pipeline in this project (`to-be-continuous/tools/gitlab-sync`) - for instance every night - to keep synchronized with the upstream project.

The job only requires a GitLab token, that shall be configured declaring a `$GITLAB_TOKEN` CI/CD project variable.
All the other parameters/variables are automatically determined by the job but might be overwritten with the appropriate project variables if need be:

| Env. Variable                    | Description                                                                                                      | Default Value                           |
| -------------------------------- | ---------------------------------------------------------------------------------------------------------------- | --------------------------------------- |
| `$SRC_GITLAB_API`                | GitLab source API url                                                                                            | `https://gitlab.com/api/v4`             |
| `$SRC_SYNC_PATH`                 | GitLab source root group path to synchronize                                                                     | `to-be-continuous`                      |
| `$DEST_GITLAB_API`               | GitLab destination API url                                                                                       | `$CI_API_V4_URL`                        |
| `$DEST_TOKEN` or `$GITLAB_TOKEN` | GitLab destination token with at least scopes `api,read_repository,write_repository` and `Owner` role            | _none_ (**mandatory**)                  |
| `$DEST_SYNC_PATH`                | GitLab destination root group path to synchronize (defaults to `--src-sync-path`)                                | determined from `$CI_PROJECT_NAMESPACE` |
| `$MAX_VISIBILITY`                | maximum visibility of projects in destination group (defaults to `public`)                                       | `$CI_PROJECT_VISIBILITY`                |
| `$SKIP_VISIBILITY`               | skip updating the destination group or project visibility (when it exists already)                               | `false`                                 |
| `$EXCLUDE`                       | project/group path(s) to exclude (multiple CLI option; env. variable is a coma separated list)                   | `samples,custom`                        |
| `$INCLUDE`                       | project/group path(s) to include (multiple CLI option; env. variable is a coma separated list)                   | `tools,docker,ansible`                  |
| `$INSECURE`                      | skip SSL verification                                                                                            | `false`                                 |
| `$UPDATE_RELEASE`                | set to force the update of the latest release (in order to trigger GitLab CI/CD catalog publication)             | `false`                                 |
| `$UPDATE_AVATAR`                 | force update the avatar images even when they exist and look the same                                            | `false`                                 |
| `$GROUP_DESCRIPTION_DISABLED`    | don't synchronize group description                                                                              | `false`                                 |
| `$PROJECT_DESCRIPTION_DISABLED`  | don't synchronize project description                                                                            | `false`                                 |
| `$NEW_GROUP_OPTIONS`             | a JSON string with [extra options for groups creation](https://docs.gitlab.com/api/groups/#create-a-group)       | _none_                                  |
| `$NEW_PROJECT_OPTIONS`           | a JSON string with [extra options for projects creation](https://docs.gitlab.com/api/projects/#create-a-project) | _none_                                  |
| `$CACHE_DIR`                     | cache directory (used to download resources such as images and Git repositories) (defaults to `.work`)           | `.work`                                 |

### Customizing copied project options

Projects are copied with the following default options:

```json
{
  "issues_access_level": "disabled",
  "merge_requests_access_level": "disabled"
}
```

You can customize those default options with your own ones with the `$NEW_PROJECT_OPTIONS` variable.
For instance if you want to globally disable CI/CD pipelines in copied projects:

```json
{
  "builds_access_level": "disabled"
}
```

Similarly, you can customize default group options with the `$NEW_GROUP_OPTIONS` variable.

## CI/CD Catalog

To use _to be continuous_ templates as CI/CD Catalog resources in your GitLab server, you'll have to manually activate the _CI/CD Catalog resource_ option (`Settings > General > Visibility > Project Features > Permissions`) in each template project.
Then you'll have to re-publish a release to make the template appear in the CI/CD Catalog.

:information_source: You may re-run the GitLab Copy CLI tool with the `--update-release` (or `$UPDATE_RELEASE` variable) set to force re-publishig the latest release of each template.
